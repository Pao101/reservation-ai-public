# OpenAI GPT Project with LangChain

Make reservation in a restaurant using LangChain and OPENAI LLM.

## Introduction

This project utilizes an LLM for natural language processing and LangChain for managing conversational workflows. The main focus is on creating a reservation chatbot implemented in a Jupyter Notebook named `reservation_chatbot.ipynb`. The project is designed to work exclusively with Jupyter notebooks and requires a `.env` file with the following environment variables:

- `OPEN_API_KEY`: Your OpenAI GPT API key.
- `ZENCHEF_AUTH_TOKEN`: Authentication token for ZenChef API.
- `ZENCHEF_RESTAURANT_ID`: Restaurant ID for ZenChef API.

## Initialization

To initialize the project, follow these steps:

1. Clone the repository.
2. Create a virtual environment:

   ```bash
   python -m venv venv
   source venv/bin/activate
   ```

3. Install the required dependencies:

   ```bash
   pip install langchain openai requests redis python-dotenv --upgrade
   ```

   Note: Make sure to install Redis and Jupyter Notebook. You can install Redis from [here](https://redis.io/download) and Jupyter Notebook using:

   ```bash
   pip install jupyter
   ```

   ````
   brew install redis
   ```

4. Install redis on your computer:
   cf https://redis.io/docs/install/install-redis/install-redis-on-mac-os/
   ```brew install redis```
   then start redis: ```redis-server```

## Running the Project

Execute the following command to run the project:

```bash
jupyter notebook reservation_chatbot.ipynb
```

Ensure that the Jupyter Notebook server is running and accessible.

## Project Structure

- `reservation_chatbot.ipynb`: Jupyter Notebook containing the project implementation.
- `.env`: Environment variables configuration file.

## Dependencies

- `langchain`: Library for managing conversational workflows.
- `openai`: OpenAI GPT library.
- `requests`: Library for making HTTP requests.
- `redis`: Redis library for caching.

## Configuration

Configure the Redis server to prevent data persistence on restarts or change the session ID for each new test.

## Usage

The notebook includes the following sections:

### 1. Installation and Setup

Install the required libraries and set up the environment variables.

```python
!pip install langchain openai requests --upgrade
!pip install redis
!pip install python-dotenv
```

### 2. Functioning

```mermaid
flowchart TB

FI{First Input} --> D
D(reservation question classifier chain) --> |Not a reservation question|DN(not a booking chain)  
D --> |Missing information for the reservation| IRnok(booking with missing info chain)
DN(not a booking chain) --> |Answer that its only purpose is reservations| D
IRnok(booking with missing info chain) ---> |Ask for missing information| D

D --> |All pieces of information are ok|ER
ER(reservation info extraction chain) --> CAP(availability checker chain)
CAP --> ACT(availability checker classifier chain)
ACT --> |Slot not available| PDi(slot proposal chain)

PDi --> ACCT(alternative choice classifier chain)

ACCT --> |No alternative slot chosen| BAD(book another date chain)

BAD --> BADC(book another date classifier chain)
BADC --> |No|RAC(reservation abandon chain)
BADC --> |Yes|D

ACCT --> |Choice of an alternative slot| CIC(customer info classifier chain)
ACT  --> |Slot available| CIC(customer info classifier chain)

CIC --> MCI(missing customer info chain)
MCI --> |Ask for missing pieces of information| CIC

CIC --> CREC(complete reservation info extraction chain)
CREC --> ZBC(Zenchef booker chain)

ZBC --> CC(client confirmation chain)
```

#### FINAL_CHAIN_WITH_HISTORY

The `final_chain_with_history` at the end of the notebook links all the chains and tool together to make the reservation happen.

### 3. Tools and Libraries

Various tools and libraries are used, such as Pydantic, Redis, and requests, to enhance the functionality of the chatbot.

### 4. Example Usage

The notebook includes examples of how to interact with the chatbot using provided prompts.

---

Feel free to modify the README according to your specific needs. If you have any additional information or specific instructions, you can include them in the appropriate sections.